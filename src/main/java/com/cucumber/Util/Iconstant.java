package com.cucumber.Util;

public interface Iconstant {
	interface GetDriver {
		public static String chrome_path = System.getProperty("user.dir") + "\\Drivers\\chromedriver.exe";
	}

	interface GetFile {
		public static String config_file_path = System.getProperty("user.dir")
				+ "\\src\\test\\resources\\ObjectRepository\\config.properties";
	}

}
