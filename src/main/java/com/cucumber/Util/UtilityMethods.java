package com.cucumber.Util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class UtilityMethods {
	public static String locVal;
	public static Properties prop;
	public static InputStream file;
	public static Properties App_Loc = UtilityMethods.LoadProperty(Iconstant.GetFile.config_file_path);

	public static String GetLocator(String key) throws IOException {
		prop = new Properties();
		try {
			file = new FileInputStream(Iconstant.GetFile.config_file_path);
		} catch (FileNotFoundException e) {
			e.fillInStackTrace();
		}
		try {
			prop.load(file);
			locVal = prop.getProperty(key);
		} catch (IOException ee) {

		}
		return locVal;

	}

	public static Properties LoadProperty(String filepath) {
		prop = new Properties();
		try {
			file = new FileInputStream(filepath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return prop;
	}

}
