package com.cucumber.steps;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;

/**
 * 
 * @author marif1
 *
 */
public class Login {

	@FindBy(id = "txtLoginName")
	@CacheLookup
	public static WebElement txtUsername;

	@FindBy(id = "txtPassword")
	@CacheLookup
	public static WebElement txtPassword;

	@FindBy(id = "btnLogin")
	@CacheLookup
	public static WebElement btnLogin;

	@Given("^user is able to see title bar on top$")
	public void user_is_able_to_see_title_bar_on_top() throws Throwable {
		System.out.println("Arif Test");
	}

	@Given("^User able to enter \"([^\"]*)\" and \"([^\"]*)\" credentials$")
	public void user_able_to_enter_and_credentials(String username, String password) throws Throwable {
		Launch.driver.findElement(By.id("txtLoginName")).sendKeys(username);
		Launch.driver.findElement(By.id("txtPassword")).sendKeys(password);
	}

	@Given("^User able to see home page$")
	public void user_able_to_see_home_page(DataTable dt) throws Throwable {
		List<Map<Object, Object>> list = dt.asMaps(Object.class, Object.class);
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).get("First Name"));
			System.out.println(list.get(i).get("Last Name"));
		}
	}
}
