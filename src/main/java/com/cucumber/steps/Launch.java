package com.cucumber.steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cucumber.Util.Iconstant;

import cucumber.api.java.en.Given;

public class Launch {
	public static WebDriver driver;
	@Given("^launch the application on Web Browser$")
	public void launch_the_application_on_Web_Browser() throws Throwable {
		System.setProperty("webdriver.chrome.driver", Iconstant.GetDriver.chrome_path);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://intranet.xavient.com/XAP/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
