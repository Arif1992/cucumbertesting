package com.cucumber.steps;

import org.openqa.selenium.By;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login_steps {
	@When("^user is able to see title bar as \"([^\"]*)\" on top$")
	public void user_is_able_to_see_title_bar_as_on_top(String title) throws Throwable {
		String tle = Launch.driver.getTitle();
		System.out.println("TTTTTTTTTTTTt: " + tle);
	}

	@Then("^User able to enter \"([^\"]*)\" and \"([^\"]*)\" credentials successfully$")
	public void user_able_to_enter_and_credentials_successfully(String username, String password) throws Throwable {
		Launch.driver.findElement(By.id("txtLoginName")).sendKeys(username);
		Launch.driver.findElement(By.id("txtPassword")).sendKeys(password);
	}

	@Then("^User able to see landing page$")
	public void user_able_to_see_landing_page() throws Throwable {
		System.out.println("TT");
	}
}
