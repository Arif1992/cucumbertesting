Feature: WebTest

  Background: start the application
    Given launch the application on Web Browser

  @001
  Scenario: TS001_TestLogin-Web - LoginTest - Verify user able to login successfully
    And user is able to see title bar on top
    And User able to enter "marif1" and "test1234" credentials
    And User able to see home page
      | First Name | Last Name | Phone No   | Password | DOB Day | DOB Month | DOB Year | Gender |
      | Abc FN     | Abc LN    | 0123123123 | Pass1234 |      01 | Jan       |     1990 | Male   |
      | Def FN     | Def LN    | 0456456456 | Abcd1234 |      01 | Feb       |     1990 | Female |
      | Xyz FN     | Xyz LN    | 0789789789 | Pass2018 |      01 | Mar       |     1990 | Female |

  @002
  Scenario: TS002_XAP-Web - LoginTest - Verify user able to login successfully
    When user is able to see title bar as "XAP" on top
    Then User able to enter "marif1" and "test1234" credentials successfully
    And User able to see landing page
