package com.cucumber.test;

class Parent {
	public static void One() {
		System.out.println("Method of class A1");
	}

	public static void Two() {
		System.out.println("Method of class A2");
	}
}

public class Child extends Parent {
	public static void Three() {
		System.out.println("Method of class TestChain");
	}

	public static void main(String[] args) {
		Parent a = new Child();
		Child b = new Child();
		Parent c = new Parent();
		// Child c=new Parent(); not allow

	}

}
