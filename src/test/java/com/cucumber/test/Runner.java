package com.cucumber.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/test-reports/cucumber-html",
		"json:target/test-reports/cucumber_json.json", "junit:target/tes t-reports/cucumber_junit.xml",
		"rerun:rerun.txt" }, monochrome = true, glue = {
				"com.cucumber.steps" }, features = { "src/main/java/com/cucumber/features/" }, tags = {"@002"}
)
public class Runner {

}
