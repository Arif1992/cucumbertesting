package com.cucumber.test;

public class TestOnly {

	public static int GetWordCount(String str, String word) {
		String a[] = str.split(" ");
		int count = 0;
		for (int i = 0; i < a.length; i++) {
			if (word.equals(a[i])) {
				count++;
			}
		}
		return count;
	}

	public static void main(String[] args) {
		System.out.println(GetWordCount("HiHi Hello Hi Hello", "Hello"));
	}
}
